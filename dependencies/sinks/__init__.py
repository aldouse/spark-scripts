from dependencies.sinks.dummy import DummySink
from dependencies.sinks.gcs import GCSSink

__all__ = ["GCSSink", "DummySink"]
