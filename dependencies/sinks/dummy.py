from pyspark.sql import DataFrame

from dependencies.sinks.sink import DataSink


class DummySink(DataSink):
    def write(self, df: DataFrame) -> None:
        df.write.mode("overwrite").csv("/tmp/dummysinkcsv.csv")
