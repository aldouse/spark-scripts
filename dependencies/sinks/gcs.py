from pyspark.sql import DataFrameWriter, SparkSession

from dependencies.options.sink_options import GCSOptions
from dependencies.sinks.sink import DataSink
from dependencies.utils.logging import Log4j


class GCSSink(DataSink):
    def __init__(
        self,
        spark: SparkSession,
        logger: Log4j,
        gcs_options: GCSOptions,
    ):
        super().__init__(spark, logger)
        self.gcs_options = gcs_options

    def write(self, df) -> None:
        writer: DataFrameWriter = (
            df.write.format(self.gcs_options.format)
            .option("path", self.gcs_options.path)
            .mode(self.gcs_options.mode)
        )

        if self.gcs_options.partition_by:
            writer = writer.partitionBy(self.gcs_options.partition_by)

        self.logger.info(f"Writing to {self.gcs_options.path}")
        writer.save()
