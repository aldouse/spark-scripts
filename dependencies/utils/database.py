"""
database utilities outside pyspark
"""

import sqlalchemy

from dependencies.utils.constants import DatabaseType


def build_connection_string(
    db_type: str,
    db_username: str,
    db_password: str,
    db_host: str,
    db_port: str,
    db_name: str,
) -> str:
    if db_type == DatabaseType.POSTGRESQL.value:
        return "postgresql+psycopg2://{db_username}:{db_password}@{db_host}:{db_port}/{db_name}".format(
            db_username=db_username,
            db_password=db_password,
            db_host=db_host,
            db_port=db_port,
            db_name=db_name,
        )
    elif db_type == DatabaseType.MYSQL.value:
        return "mysql+mysqlconnector://{db_username}:{db_password}@{db_host}:{db_port}/{db_name}".format(
            db_username=db_username,
            db_password=db_password,
            db_host=db_host,
            db_port=db_port,
            db_name=db_name,
        )
    elif db_type == DatabaseType.SQLSERVER.value:
        raise NotImplementedError("sql server not implemented")
    else:
        raise Exception(f"database type {db_type} not supported")


def query_and_fetch_one(connection_string: str, query: str) -> dict:
    engine = sqlalchemy.create_engine(connection_string)

    with engine.connect() as conn:
        with conn.execute(sqlalchemy.text(query)) as cursor_result:
            row = cursor_result.fetchone()
            if row:
                return row._asdict()

    return {}
