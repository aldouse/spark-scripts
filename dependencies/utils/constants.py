from enum import Enum

PROCESSED_TIMESTAMP = "processed_timestamp"

SQL_ESCAPE_CHAR = {
    "mysql": "`",
    "postgresql": '"',
    "sqlserver": '"',
}


class DatabaseType(Enum):
    MYSQL = "mysql"
    POSTGRESQL = "postgresql"
    SQLSERVER = "sqlserver"
