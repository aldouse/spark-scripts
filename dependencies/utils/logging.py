class Log4j(object):
    """Wrapper class for Log4j JVM object.

    :param spark: SparkSession object.
    """

    def __init__(self, spark):
        # get spark app details with which to prefix all messages
        conf = spark.sparkContext.getConf()
        app_id: str = conf.get("spark.app.id")
        app_name: str = conf.get("spark.app.name")

        log4j = spark._jvm.org.apache.log4j
        message_prefix: str = "<" + app_name + " " + app_id + ">"
        self.logger = log4j.LogManager.getLogger(message_prefix)

    def error(self, message: str) -> None:
        """Log an error.

        :param: Error message to write to log
        :return: None
        """
        self.logger.error(message)
        return None

    def warn(self, message: str) -> None:
        """Log a warning.

        :param: Warning message to write to log
        :return: None
        """
        self.logger.warn(message)
        return None

    def info(self, message: str) -> None:
        """Log information.

        :param: Information message to write to log
        :return: None
        """
        self.logger.info(message)
        return None
