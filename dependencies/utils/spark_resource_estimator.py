import logging

from dependencies.options.extras import (
    SparkResourceEstimatorMode,
    SparkResourceEstimatorOptions,
)
from dependencies.options.source_options import JDBCOptions
from dependencies.utils.database import (
    DatabaseType,
    build_connection_string,
    query_and_fetch_one,
)


class JDBCSourceSparkResourceEstimator:
    fetch_size: int = 100000

    def __init__(
        self,
        jdbc_options: JDBCOptions,
        sre_options: SparkResourceEstimatorOptions,
        query: str,
    ) -> None:
        self.jdbc_options = jdbc_options
        self.sre_options = sre_options
        self.query = query

        self.connection_string = build_connection_string(
            jdbc_options.db_type,
            jdbc_options.db_username,
            jdbc_options.db_password,
            jdbc_options.db_host,
            jdbc_options.db_port,
            jdbc_options.db_name,
        )

    def get_spark_conf(self) -> dict:
        if self.sre_options.mode == SparkResourceEstimatorMode.INCREMENTAL_JOB:
            return self._run_incremental_job_estimation()
        elif self.sre_options.mode == SparkResourceEstimatorMode.FULL_LOAD_JOB:
            return self._run_full_load_job_estimation()
        elif self.sre_options.mode == SparkResourceEstimatorMode.CUSTOM_JOB:
            return self._run_custom_job_estimation()

        return {}

    def _run_incremental_job_estimation(self) -> dict:
        # MIGHTDO: implement incremental mode estimation if needed
        # - get row count and query size estimation from bigquery past ingestion data ( FROM datalake.INFORMATION_SCHEMA.PARTITIONS )
        raise NotImplementedError

    def _run_full_load_job_estimation(self) -> dict:
        # MIGHTDO: implement full load mode estimation if needed
        # - get row count estimation
        #   postgresql:
        #       SELECT
        #       (reltuples/relpages) * (
        #         pg_relation_size('<table_name>') /
        #         (current_setting('block_size')::integer)
        #       )
        #       FROM pg_class where relname = '<table_name>';
        #   mysql:
        #       SELECT DATA_LENGTH/AVG_ROW_LENGTH FROM INFORMATION_SCHEMA.TABLES
        #       WHERE TABLE_NAME = '<table_name>';
        # - or get full table size
        raise NotImplementedError

    def _get_table_size(self) -> float:
        if self.jdbc_options.db_type == DatabaseType.POSTGRESQL.value:
            size_query: str = f"""
            SELECT DISTINCT
            pg_relation_size(relid) / 1024 / 1024 / 1024 as table_size_gb
            FROM pg_stat_all_indexes i JOIN pg_class c ON i.relid=c.oid 
            WHERE i.relname='{self.jdbc_options.table_name}';
            """
        elif self.jdbc_options.db_type == DatabaseType.MYSQL.value:
            size_query: str = f"""
            select round(1.0*data_length/1024/1024/1024, 2) as table_size_gb
            from information_schema.tables
            where table_schema not in('information_schema', 'mysql',
                                      'sys', 'performance_schema')
                  and table_schema = '{self.jdbc_options.db_name}'
                  and table_name = '{self.jdbc_options.table_name}';
            """
        else:
            raise NotImplementedError

        res: dict = query_and_fetch_one(self.connection_string, size_query)

        if res:
            return res.get("table_size_gb", 0.0)

        return 0.0

    def _get_partition_column(self) -> str:
        if self.jdbc_options.db_type == DatabaseType.POSTGRESQL.value:
            pk_query: str = f"""
            SELECT a.attname AS pk_column, format_type(a.atttypid, a.atttypmod) AS data_type
            FROM   pg_index i
            JOIN   pg_attribute a ON a.attrelid = i.indrelid
                                 AND a.attnum = ANY(i.indkey)
            WHERE  i.indrelid = '{self.jdbc_options.table_name}'::regclass
            AND    i.indisprimary;
            """
        elif self.jdbc_options.db_type == DatabaseType.MYSQL.value:
            pk_query: str = f"""
            select 
              column_name as pk_column, 
              data_type as data_type 
            from 
              information_schema.columns 
            where 
              table_schema = '{self.jdbc_options.db_name}' 
              and table_name = '{self.jdbc_options.table_name}' 
              and column_key = 'PRI';
            """
        else:
            raise NotImplementedError

        res: dict = query_and_fetch_one(self.connection_string, pk_query)

        if res:
            # TODO: improve data type checking logic
            if "int" in res["data_type"]:
                return res["pk_column"]
        return ""

    def _get_row_count(self) -> int:
        row_count_multiplier: float = 1.0
        if self.jdbc_options.query_filter:
            count_query: str = f"""
            SELECT COUNT(*) row_count FROM {self.jdbc_options.table_name}
            WHERE {self.jdbc_options.query_filter}
            """
        else:
            if self.jdbc_options.db_type == DatabaseType.POSTGRESQL.value:
                count_query: str = f"""
                SELECT
                  (reltuples / relpages) * (
                    pg_relation_size('{self.jdbc_options.table_name}') / (current_setting('block_size') :: integer)
                  ) row_count
                FROM
                  pg_class
                where
                  relname = '{self.jdbc_options.table_name}';
                """
            elif self.jdbc_options.db_type == DatabaseType.MYSQL.value:
                count_query: str = f"""
                SELECT DATA_LENGTH / AVG_ROW_LENGTH AS row_count FROM INFORMATION_SCHEMA.TABLES
                WHERE TABLE_SCHEMA = '{self.jdbc_options.db_name}' AND TABLE_NAME = '{self.jdbc_options.table_name}';
                """
            else:
                raise NotImplementedError
            row_count_multiplier = 1.3

        result: dict = query_and_fetch_one(self.connection_string, count_query)

        if result:
            return int(int(result.get("row_count", 0)) * row_count_multiplier)

        return 0

    def _run_custom_job_estimation(self) -> dict:
        row_count: int = self._get_row_count()

        # if row count is 0, return lowest possible spec
        if row_count == 0:
            return {
                "spark.executor.memory": "1g",
                "spark.executor.cores": 1,
                "spark.cores.max": 1,
                "spark_reader.partitionColumn": "",
                "spark_reader.numPartitions": 1,
                "spark_reader.fetchSize": self.fetch_size,
            }

        partition_column: str = self._get_partition_column()

        # if usable partition column is not available, use table size
        # as executor memory estimation
        # 0.6 is the default value for spark.memory.fraction
        if not partition_column:
            table_size: float = self._get_table_size()

            executor_memory: str = "16g"

            # currently highest memory in one worker is 16gb
            for x in range(1, 17):
                if table_size < x * 0.6:
                    executor_memory = str(x) + "g"
                    break

            return {
                "spark.executor.memory": executor_memory,
                "spark.executor.cores": 2,
                "spark.cores.max": 4,
                "spark_reader.partitionColumn": "",
                "spark_reader.numPartitions": 50,
                "spark_reader.fetchSize": self.fetch_size,
            }

        return {
            "spark.executor.memory": "2g",
            "spark.executor.cores": 1,
            "spark.cores.max": 4,
            "spark_reader.partitionColumn": partition_column,
            "spark_reader.numPartitions": int(row_count / self.fetch_size),
            "spark_reader.fetchSize": self.fetch_size,
        }
