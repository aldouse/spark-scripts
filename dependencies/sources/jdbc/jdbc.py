from abc import abstractmethod, abstractproperty
from typing import Tuple

from pyspark.sql import DataFrame, DataFrameReader, Row, SparkSession

from dependencies.options import JDBCOptions
from dependencies.sources.source import DataSource
from dependencies.utils.logging import Log4j


class JDBCSource(DataSource):
    @abstractproperty
    def spark_jdbc_driver(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def _construct_table_name(self) -> str:
        raise NotImplementedError

    def __init__(
        self,
        spark: SparkSession,
        logger: Log4j,
        jdbc_options: JDBCOptions,
    ):
        super().__init__(spark, logger)
        self.jdbc_options = jdbc_options
        self._spark_reader = self._setup_base_reader()

    def _construct_db_url(self) -> str:
        return (
            f"jdbc:{self.jdbc_options.db_type}"
            f"://{self.jdbc_options.db_host}"
            f":{self.jdbc_options.db_port}"
            f"/{self.jdbc_options.db_name}"
            f"?user={self.jdbc_options.db_username}"
            f"&password={self.jdbc_options.db_password}"
        )

    def _setup_base_reader(self) -> DataFrameReader:
        reader: DataFrameReader = (
            self.spark.read.format("jdbc")
            .option("url", self._construct_db_url())
            .option("queryTimeout", self.jdbc_options.query_timeout_seconds)
            .option("fetchSize", self.jdbc_options.fetch_size)
            .option("driver", self.spark_jdbc_driver)
        )
        return reader

    def _add_partition_opts(self, reader: DataFrameReader) -> DataFrameReader:
        if not self.jdbc_options.partition_column:
            return reader

        lower_bound, upper_bound = self._get_partition_boundaries()

        reader = (
            reader.option(
                "partitionColumn",
                self.jdbc_options.partition_column,
            )
            .option("lowerBound", lower_bound)
            .option("upperBound", upper_bound)
            .option("numPartitions", self.jdbc_options.num_partitions)
        )

        return reader

    def _construct_partition_boundaries_query(self) -> str:
        # TODO: add query parser to get filter from query variable
        # instead of separating it into different variable
        where_clause: str = "WHERE " + (
            self.jdbc_options.query_filter if self.jdbc_options.query_filter is not None else ""
        )
        return f"""
        SELECT
            min({self.jdbc_options.partition_column}) lower_bound,
            max({self.jdbc_options.partition_column}) upper_bound
        FROM
            {self._construct_table_name()}
        {where_clause if len(where_clause) > 6 else ""}
        """

    def _get_partition_boundaries(self) -> Tuple[str, str]:
        query: str = self._construct_partition_boundaries_query()
        boundaries: Row = self.run_query(query).collect()[0]

        return boundaries["lower_bound"], boundaries["upper_bound"]

    def run_query(self, query: str) -> DataFrame:
        return (
            self.spark.read.format("jdbc")
            .option("url", self._construct_db_url())
            .option("queryTimeout", self.jdbc_options.query_timeout_seconds)
            .option("driver", self.spark_jdbc_driver)
            .option("query", query)
        ).load()

    def read(self) -> DataFrame:
        reader = self._add_partition_opts(self._spark_reader)
        reader = reader.option("dbtable", f"({self.jdbc_options.query}) foo")

        self.logger.info(f"running query: {self.jdbc_options.query}")
        return reader.load()
