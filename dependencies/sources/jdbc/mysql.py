from dependencies.sources.jdbc.jdbc import JDBCSource


class JDBCMysqlSource(JDBCSource):
    spark_jdbc_driver: str = "com.mysql.cj.jdbc.Driver"

    def _construct_table_name(self) -> str:
        if "." not in self.jdbc_options.table_name:
            return f"`{self.jdbc_options.db_name}`.{self.jdbc_options.table_name}"
        return self.jdbc_options.table_name
