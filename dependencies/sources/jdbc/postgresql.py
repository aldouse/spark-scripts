from dependencies.sources.jdbc.jdbc import JDBCSource


class JDBCPostgresqlSource(JDBCSource):
    spark_jdbc_driver: str = "org.postgresql.Driver"

    def _construct_table_name(self) -> str:
        if "." not in self.jdbc_options.table_name:
            return f"public.{self.jdbc_options.table_name}"
        return self.jdbc_options.table_name
