from dependencies.sources.jdbc.jdbc import JDBCSource


class JDBCSqlServerSource(JDBCSource):
    spark_jdbc_driver: str = "com.microsoft.sqlserver.jdbc.SQLServerDriver"

    def _construct_table_name(self) -> str:
        return self.jdbc_options.table_name

    def _construct_db_url(self) -> str:
        parameters: str = ";".join([ f"{key}={value}" for key, value in self.jdbc_options.db_parameters.items()])
        # jdbc:sqlserver://host:1433;user=usr;password=pwd;database=dbname;encrypt=true;trustServerCertification=true
        return (
            f"jdbc:{self.jdbc_options.db_type}"
            f"://{self.jdbc_options.db_host}"
            f":{self.jdbc_options.db_port}"
            f";{parameters}"
        )
