from abc import ABC, abstractmethod

from pyspark.sql import DataFrame, SparkSession

from dependencies.utils.logging import Log4j


class DataSource(ABC):
    def __init__(self, spark: SparkSession, logger: Log4j):
        self.spark = spark
        self.logger = logger

    @abstractmethod
    def read(self) -> DataFrame:
        raise NotImplementedError
