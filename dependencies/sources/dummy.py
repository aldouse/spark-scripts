from faker import Faker
from pyspark.sql import DataFrame, SparkSession

from dependencies.sources.source import DataSource
from dependencies.utils.logging import Log4j


class DummySource(DataSource):
    def __init__(self, spark: SparkSession, logger: Log4j, row_count: int = 100):
        super().__init__(spark, logger)
        self.fake = Faker(["id_ID"])
        self.row_count = row_count

    def read(self) -> DataFrame:
        data = []

        for _ in range(0, self.row_count):
            data.append(
                (
                    self.fake.unique.random_int(),
                    self.fake.name(),
                    self.fake.address(),
                    self.fake.email(),
                )
            )

        return self.spark.createDataFrame(data, ["id", "name", "address", "email"])
