from dataclasses import dataclass, field
from typing import Optional


@dataclass
class GCSOptions:
    """Arguments for Google Cloud Storage related operations"""

    format: str  # saved file format in destination
    path: str  # full object path, e.g. gs://bucket/spark
    mode: str  # spark write mode [overwrite, append, ignore, errorifexists]
    partition_by: list[str] = field(
        default_factory=lambda: []
    )  # column used for splitting files into smaller sizes

    def __post_init__(self):
        if not self.path.startswith("gs://"):
            self.path = f"gs://{self.path}"
