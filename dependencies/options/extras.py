from dataclasses import dataclass
from enum import Enum


class SparkResourceEstimatorMode(Enum):
    INCREMENTAL_JOB = "incremental"
    FULL_LOAD_JOB = "full_load"
    CUSTOM_JOB = "custom"


@dataclass
class SparkResourceEstimatorOptions:
    """Arguments for spark resource estimator utils"""

    mode: SparkResourceEstimatorMode = (
        SparkResourceEstimatorMode.CUSTOM_JOB
    )  # estimator mode to run
