import enum
from dataclasses import dataclass
from typing import Optional

import yaml
from google.cloud import secretmanager


class DbCredentialSourceType(enum.Enum):
    LOCAL = "LOCAL"
    GSM = "GSM"
    ARG = "ARG"


@dataclass
class JDBCOptions:
    """List of arguments for JDBCOptions"""

    # required user specified inputs
    db_name: str  # database name
    table_name: str  # table name to select from

    # optional user specified inputs with custom constraint logic

    db_credential_source: DbCredentialSourceType = DbCredentialSourceType.ARG

    db_credential_path: str = ""  # db credential path
    db_credential_secret_name: str = (
        ""  # projects/<project_id>/secrets/<secret_id>/versions/<version>
    )

    db_username: str = ""  # database username
    db_password: str = ""  # database password
    db_type: str = ""  # database type. e.g. mysql postgresql
    db_host: str = ""  # database host
    db_port: str = ""  # database port

    db_parameters: dict = ""  # extra params, handles differently for each dbtype

    query: str = ""  # full query to run in database

    # spark reader config
    query_timeout_seconds: str = (
        "1800"  # query timeout in seconds for spark reader arg # noqa: E501
    )

    fetch_size: str = "100000"  # fetch size for spark reader arg

    # partitioning config
    partition_column: Optional[
        str
    ] = None  # an indexed column name used for partition # noqa: E501
    num_partitions: Optional[
        str
    ] = None  # number of partitions for spark reaer arg # noqa: E501
    query_filter: Optional[  # string of where clause from query that will be used for partitioning purposes # noqa: E501
        str
    ] = None

    def __post_init__(self):
        if self.db_credential_source == DbCredentialSourceType.LOCAL:
            self._load_db_credential_from_path()
        elif self.db_credential_source == DbCredentialSourceType.GSM:
            self._load_db_credential_from_gsm()

        if not self.db_username:
            raise AttributeError("Field db_username cannot be empty")
        if not self.db_password:
            raise AttributeError("Field db_password cannot be empty")
        if not self.db_type:
            raise AttributeError("Field db_type cannot be empty")
        if not self.db_host:
            raise AttributeError("Field db_host cannot be empty")
        if not self.db_port:
            raise AttributeError("Field db_port cannot be empty")

        if not self.query:
            self.query = f"SELECT * FROM {self.table_name}"

        if self.partition_column:
            if not self.num_partitions:
                raise AttributeError(
                    "Field 'num_partitions' cannot be empty when partition_column is not None"  # noqa: E501
                )

    def _load_db_credential_from_path(self):
        with open(self.db_credential_path, "r") as f:
            creds = yaml.safe_load(f)
        self.db_username = creds[self.db_name]["parameters"]["user"]
        self.db_password = creds[self.db_name]["parameters"]["password"]
        self.db_type = creds[self.db_name]["type"]
        self.db_host = creds[self.db_name]["host"]
        self.db_port = str(creds[self.db_name]["port"])
        self.db_parameters = creds[self.db_name]["parameters"]

    def _load_db_credential_from_gsm(self):
        client = secretmanager.SecretManagerServiceClient()
        response = client.access_secret_version(name=self.db_credential_secret_name)

        creds = yaml.safe_load(response.payload.data.decode("UTF-8"))
        self.db_username = creds[self.db_name]["parameters"]["user"]
        self.db_password = creds[self.db_name]["parameters"]["password"]
        self.db_type = creds[self.db_name]["type"]
        self.db_host = creds[self.db_name]["host"]
        self.db_port = str(creds[self.db_name]["port"])
        self.db_parameters = creds[self.db_name]["parameters"]
