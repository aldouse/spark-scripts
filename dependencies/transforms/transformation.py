from abc import ABC, abstractmethod

from pyspark.sql import DataFrame

from dependencies.utils.logging import Log4j


class Transformation(ABC):
    def __init__(self, logger: Log4j):
        self.logger = logger

    @abstractmethod
    def transform(self, DataFrame) -> DataFrame:
        raise NotImplementedError
