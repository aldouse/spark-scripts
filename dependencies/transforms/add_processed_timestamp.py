from datetime import datetime

from pyspark.sql import DataFrame
from pyspark.sql.functions import lit, to_timestamp

from dependencies.transforms.transformation import Transformation
from dependencies.utils.constants import PROCESSED_TIMESTAMP
from dependencies.utils.logging import Log4j


class AddProcessedTimestamp(Transformation):
    def __init__(self, logger: Log4j, processed_timestamp: datetime):
        super().__init__(logger)
        self.processed_timestamp = processed_timestamp

    def transform(self, df: DataFrame) -> DataFrame:
        return df.withColumn(
            PROCESSED_TIMESTAMP,
            to_timestamp(lit(self.processed_timestamp.isoformat())),
        )
