from pyspark.sql import DataFrame, SparkSession

from dependencies.sinks.sink import DataSink
from dependencies.sources.source import DataSource
from dependencies.transforms.transformation import Transformation
from dependencies.utils.logging import Log4j


class SimpleJob:
    def __init__(
        self,
        spark: SparkSession,
        logger: Log4j,
        data_source: DataSource,
        data_sink: DataSink,
        transforms: list[Transformation] = [],
    ):
        self.spark = spark
        self.logger = logger
        self.data_source = data_source
        self.data_sink = data_sink
        self.transforms = transforms

    def run(self):
        df: DataFrame = self.data_source.read()

        transformed_df: DataFrame = df

        for tf in self.transforms:
            transformed_df = tf.transform(transformed_df)

        self.data_sink.write(transformed_df)
