db_to_gcs.py 
```bash
spark-submit \
    --master spark://localhost:7077 \
    --name app \
    --total-executor-cores 1 \
    --executor-cores 1 \
    --executor-memory 1g \
    --driver-memory 1g \
    /workspace/jobs/db_to_gcs.py \
    [ARGS]
```
```
$ python jobs/db_to_gcs.py --help
usage: db_to_gcs.py [-h] [--timestamp_column str] [--timestamp_filter fromisoformat] [--interval_in_hour str] [--processed_timestamp fromisoformat] [--use_resource_estimator str] --jdbc_db_name str
                    --jdbc_table_name str [--jdbc_db_credential_source DbCredentialSourceType] [--jdbc_db_credential_path str] [--jdbc_db_credential_secret_name str] [--jdbc_db_username str]
                    [--jdbc_db_password str] [--jdbc_db_type str] [--jdbc_db_host str] [--jdbc_db_port str] [--jdbc_db_parameters dict] [--jdbc_query str] [--jdbc_query_timeout_seconds str]
                    [--jdbc_fetch_size str] [--jdbc_partition_column [str]] [--jdbc_num_partitions [str]] [--jdbc_query_filter [str]] --gcs_format str --gcs_path str --gcs_mode str [--gcs_partition_by list]
                    [--sre_mode SparkResourceEstimatorMode]

optional arguments:
  -h, --help            show this help message and exit
  --timestamp_column str
                        column_name that will be used for interval based filtering, if left empty will not generate where clause (default: )
  --timestamp_filter fromisoformat
                        timestamp that will be used in filtering provided timestamp_column (default: 2023-11-15 14:16:47.779662)
  --interval_in_hour str
                        hour interval that will be used to filter the upper ceiling of timestamp_filter (default: 6)
  --processed_timestamp fromisoformat
                        timestamp of spark job run time (default: 2023-11-15 14:16:47.779706)
  --use_resource_estimator str
                        if set to True, specified spark specification will be overridden by resource estimator (default: false)

JDBCOptions ['jdbc_options']:
  List of arguments for JDBCOptions

  --jdbc_db_name str    required user specified inputs (default: None)
  --jdbc_table_name str
                        table name to select from (default: None)
  --jdbc_db_credential_source DbCredentialSourceType
                        optional user specified inputs with custom constraint logic (default: ARG)
  --jdbc_db_credential_path str
                        db credential path (default: )
  --jdbc_db_credential_secret_name str
                        (default: )
  --jdbc_db_username str
                        projects/<project_id>/secrets/<secret_id>/versions/<version> (default: )
  --jdbc_db_password str
                        database password (default: )
  --jdbc_db_type str    database type. e.g. mysql postgresql (default: )
  --jdbc_db_host str    database host (default: )
  --jdbc_db_port str    database port (default: )
  --jdbc_db_parameters dict
                        extra params, handles differently for each dbtype (default: )
  --jdbc_query str      full query to run in database (default: )
  --jdbc_query_timeout_seconds str
                        spark reader config (default: 1800)
  --jdbc_fetch_size str
                        query timeout in seconds for spark reader arg # noqa: E501 (default: 100000)
  --jdbc_partition_column [str]
                        partitioning config (default: None)
  --jdbc_num_partitions [str]
                        an indexed column name used for partition # noqa: E501 (default: None)
  --jdbc_query_filter [str]
                        number of partitions for spark reaer arg # noqa: E501 (default: None)

GCSOptions ['gcs_options']:
  Arguments for Google Cloud Storage related operations

  --gcs_format str      saved file format in destination (default: None)
  --gcs_path str        full object path, e.g. gs://bucket/spark (default: None)
  --gcs_mode str        spark write mode [overwrite, append, ignore, errorifexists] (default: None)
  --gcs_partition_by list
                        (default: [])

SparkResourceEstimatorOptions ['sre_options']:
  Arguments for spark resource estimator utils

  --sre_mode SparkResourceEstimatorMode
                        (default: CUSTOM_JOB)
```
