{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  packages = with pkgs; [
    python39
    poetry
  ];
  # shellHook = ''
  #   export PIP_PREFIX=$(pwd)/_build/pip_packages
  #   export PYTHONPATH="$PIP_PREFIX/${pkgs.python3.sitePackages}:$PYTHONPATH"
  #   export PATH="$PIP_PREFIX/bin:$PATH"
  #   unset SOURCE_DATE_EPOCH
  # '';
  LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath [ pkgs.stdenv.cc.cc ];
}
