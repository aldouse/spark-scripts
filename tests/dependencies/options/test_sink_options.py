import pytest

from dependencies.options import GCSOptions


def test_gcs_options():
    gcs_options = GCSOptions(
        format="parquet",
        path="gs://test",
        mode="overwrite",
        partition_by=["updated_at"],
    )

    assert gcs_options is not None


def test_gcs_options_nongs_path():
    gcs_options = GCSOptions(
        format="parquet",
        path="test",
        mode="overwrite",
        partition_by=["updated_at"],
    )

    assert gcs_options.path == "gs://test"
