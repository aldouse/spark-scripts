import pytest

from dependencies.options import JDBCOptions


def test_jdbc_options():
    jdbc_options = JDBCOptions(
        db_name="test-db",
        table_name="test_table",
        load_db_credential_from_path=False,
        db_username="test_username",
        db_password="test_password",
        db_type="mysql",
        db_host="localhost",
        db_port="3306",
        query="select * from test_table",
    )
    assert jdbc_options is not None


def test_jdbc_options_empty_query():
    jdbc_options = JDBCOptions(
        db_name="test-db",
        table_name="test_table",
        load_db_credential_from_path=False,
        db_username="test_username",
        db_password="test_password",
        db_type="mysql",
        db_host="localhost",
        db_port="3306",
    )

    assert jdbc_options.query == "SELECT * FROM test_table"


def test_jdbc_options_num_partitions_none_error():
    with pytest.raises(AttributeError):
        _ = JDBCOptions(
            db_name="test-db",
            table_name="test_table",
            load_db_credential_from_path=False,
            db_username="test_username",
            db_password="test_password",
            db_type="mysql",
            db_host="localhost",
            db_port="3306",
            query="select * from test_table",
            partition_column="updated_at",
            num_partitions=None,
        )


def test_jdbc_options_load_db_credential(mocker):
    mocked_db_credentials_file = mocker.mock_open(
        read_data="""
test-db:
    type: mysql
    host: localhost
    port: 3306
    parameters:
        user: test_user
        password: test_password
        """
    )

    mocker.patch("builtins.open", mocked_db_credentials_file)

    jdbc_options = JDBCOptions(
        db_name="test-db",
        table_name="test_table",
        load_db_credential_from_path=True,
        db_credential_path="/test/path/credential",
        query="select * from test_table",
    )

    assert jdbc_options.db_username == "test_user"
    assert jdbc_options.db_password == "test_password"
    assert jdbc_options.db_type == "mysql"
    assert jdbc_options.db_host == "localhost"
    assert jdbc_options.db_port == "3306"


def test_jdbc_options_db_username_attributeerror():
    with pytest.raises(AttributeError):
        _ = JDBCOptions(
            db_name="test-db",
            table_name="test_table",
            load_db_credential_from_path=False,
            db_password="test_password",
            db_type="mysql",
            db_host="localhost",
            db_port="3306",
            query="select * from test_table",
        )
    with pytest.raises(AttributeError):
        _ = JDBCOptions(
            db_name="test-db",
            table_name="test_table",
            load_db_credential_from_path=False,
            db_username="test_username",
            db_type="mysql",
            db_host="localhost",
            db_port="3306",
            query="select * from test_table",
        )
    with pytest.raises(AttributeError):
        _ = JDBCOptions(
            db_name="test-db",
            table_name="test_table",
            load_db_credential_from_path=False,
            db_username="test_username",
            db_password="test_password",
            db_host="localhost",
            db_port="3306",
            query="select * from test_table",
        )
    with pytest.raises(AttributeError):
        _ = JDBCOptions(
            db_name="test-db",
            table_name="test_table",
            load_db_credential_from_path=False,
            db_username="test_username",
            db_password="test_password",
            db_type="mysql",
            db_port="3306",
            query="select * from test_table",
        )
    with pytest.raises(AttributeError):
        _ = JDBCOptions(
            db_name="test-db",
            table_name="test_table",
            load_db_credential_from_path=False,
            db_username="test_username",
            db_password="test_password",
            db_type="mysql",
            db_host="localhost",
            query="select * from test_table",
        )
