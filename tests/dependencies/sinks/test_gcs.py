import pytest

from dependencies.options import GCSOptions
from dependencies.sinks import GCSSink


@pytest.fixture
def gcs_options():
    return GCSOptions(
        format="parquet",
        path="gs://test/test/test",
        mode="overwrite",
        partition_by=["id"],
    )


def test_gcs_sink(spark_helper, gcs_options, mocker):
    gcs_sink = GCSSink(
        spark_helper.session,
        spark_helper.logger,
        gcs_options,
    )

    mocker.patch("pyspark.sql.DataFrameWriter.save", return_value=None)
    df = spark_helper.session.createDataFrame([(0, 10)], ["a", "b"])

    gcs_sink.write(df)
