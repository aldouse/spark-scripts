from datetime import datetime

import pytest

from dependencies.transforms import AddProcessedTimestamp
from dependencies.utils.constants import PROCESSED_TIMESTAMP


@pytest.fixture
def add_processed_timestamp(spark_helper):
    tf = AddProcessedTimestamp(
        spark_helper.logger,
        datetime.now(),
    )
    return tf


def test_add_processed_timestamp_transform(spark_helper, add_processed_timestamp):
    df = spark_helper.session.createDataFrame([(0, 10)], ["a", "b"])
    transformed_df = add_processed_timestamp.transform(df)

    assert PROCESSED_TIMESTAMP in transformed_df.columns
