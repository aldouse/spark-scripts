from dependencies.jobs import SimpleJob
from dependencies.sinks import DummySink
from dependencies.sources import DummySource


def test_simple_job(spark_helper, mocker):
    mocker.patch("pyspark.sql.DataFrameWriter.csv", return_value=None)
    dummy_source = DummySource(spark_helper.session, spark_helper.logger)
    dummy_sink = DummySink(spark_helper.session, spark_helper.logger)

    simple_job = SimpleJob(
        spark_helper.session,
        spark_helper.logger,
        data_source=dummy_source,
        data_sink=dummy_sink,
    )

    simple_job.run()
