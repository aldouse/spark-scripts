import pytest

from dependencies.options import JDBCOptions, SparkResourceEstimatorOptions
from dependencies.utils.spark_resource_estimator import JDBCSourceSparkResourceEstimator


@pytest.fixture
def jdbc_source_sre():
    return JDBCSourceSparkResourceEstimator(
        JDBCOptions(
            db_username="testuser",
            db_password="testpassword",
            db_type="mysql",
            db_host="testhost",
            db_port="3306",
            db_name="testdb",
            table_name="testtable",
            query="select * from testtable where id < 100",
            query_filter="id < 100",
            partition_column="id",
            num_partitions="10",
        ),
        SparkResourceEstimatorOptions(),
        "select * from testtable where id < 100",
    )


def test__run_custom_job_estimation_normal_case(jdbc_source_sre, mocker):
    patched_module_prefix = (
        "dependencies.utils.spark_resource_estimator.JDBCSourceSparkResourceEstimator."
    )
    mocker.patch(
        f"{patched_module_prefix}_get_row_count",
        return_value=10000000,
    )
    mocker.patch(
        f"{patched_module_prefix}_get_partition_column",
        return_value="id",
    )

    estimation_result: dict = jdbc_source_sre._run_custom_job_estimation()

    assert {
        "spark.cores.max": 4,
        "spark.executor.cores": 1,
        "spark.executor.memory": "2g",
        "spark_reader.fetchSize": 100000,
        "spark_reader.numPartitions": 100,
        "spark_reader.partitionColumn": "id",
    } == estimation_result


def test__run_custom_job_estimation_no_partition_column_case(jdbc_source_sre, mocker):
    patched_module_prefix = (
        "dependencies.utils.spark_resource_estimator.JDBCSourceSparkResourceEstimator."
    )
    mocker.patch(
        f"{patched_module_prefix}_get_row_count",
        return_value=10000000,
    )
    mocker.patch(
        f"{patched_module_prefix}_get_table_size",
        return_value=4.5,
    )
    mocker.patch(
        f"{patched_module_prefix}_get_partition_column",
        return_value="",
    )

    estimation_result: dict = jdbc_source_sre._run_custom_job_estimation()

    assert {
        "spark.cores.max": 4,
        "spark.executor.cores": 2,
        "spark.executor.memory": "8g",
        "spark_reader.fetchSize": 100000,
        "spark_reader.numPartitions": 50,
        "spark_reader.partitionColumn": "",
    } == estimation_result
