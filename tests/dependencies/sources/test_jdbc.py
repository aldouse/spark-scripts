import pytest

from dependencies.options import JDBCOptions
from dependencies.sources import JDBCMysqlSource, JDBCPostgresqlSource


@pytest.fixture
def mysql_jdbc_options():
    return JDBCOptions(
        db_username="testuser",
        db_password="testpassword",
        db_type="mysql",
        db_host="testhost",
        db_port="3306",
        db_name="testdb",
        table_name="testtable",
        query="select id, col1, col2 from testtable where id < 100",
        query_filter="id <= 100",
        partition_column="id",
        num_partitions="10",
    )


@pytest.fixture
def postgresql_jdbc_options():
    return JDBCOptions(
        db_username="testuser",
        db_password="testpassword",
        db_type="postgresql",
        db_host="testhost",
        db_port="3306",
        db_name="testdb",
        table_name="testtable",
        query="select id, col1, col2 from testtable where id <= 100",
        query_filter="id <= 100",
        partition_column="id",
        num_partitions="10",
    )


@pytest.fixture
def mysql_source(spark_helper, mysql_jdbc_options):
    return JDBCMysqlSource(
        spark_helper.session,
        spark_helper.logger,
        mysql_jdbc_options,
    )


def test_jdbc_mysql_source(spark_helper, mysql_jdbc_options):
    mysql_source = JDBCMysqlSource(
        spark_helper.session,
        spark_helper.logger,
        mysql_jdbc_options,
    )

    assert mysql_source.jdbc_options == mysql_jdbc_options
    assert mysql_source._spark_reader is not None


def test_jdbc_postgresql_source(spark_helper, postgresql_jdbc_options):
    postgresql_source = JDBCPostgresqlSource(
        spark_helper.session,
        spark_helper.logger,
        postgresql_jdbc_options,
    )

    assert postgresql_source.jdbc_options == postgresql_jdbc_options
    assert postgresql_source._spark_reader is not None


def test_get_partition_boundaries(spark_helper, mysql_source, mocker):
    data = [(0, 10)]
    data_df = spark_helper.session.createDataFrame(data, ["lower_bound", "upper_bound"])

    mocker.patch(
        "dependencies.sources.jdbc.jdbc.JDBCSource.run_query",
        return_value=data_df,
    )

    lower_bound, upper_bound = mysql_source._get_partition_boundaries()

    assert lower_bound == 0
    assert upper_bound == 10


def test_add_partition_opts(mysql_source, mocker):
    mocker.patch(
        "dependencies.sources.jdbc.jdbc.JDBCSource._get_partition_boundaries",
        return_value=(0, 10),
    )

    reader = mysql_source._add_partition_opts(mysql_source._spark_reader)
    assert reader is not None


def test_add_partition_opts_none(mysql_source):
    mysql_source.jdbc_options.partition_column = None

    reader = mysql_source._add_partition_opts(mysql_source._spark_reader)

    assert reader == mysql_source._spark_reader


# def test_construct_partition_boundaries_query(mysql_source):
#     got = mysql_source._construct_partition_boundaries_query()
#     want = f"""
#     SELECT
#         min(id) lower_bound,
#         max(id) upper_bound
#     FROM
#         testdb.testtable
#     WHERE
#         id <= 100
#     """

#     print(got)

#     assert got == want
