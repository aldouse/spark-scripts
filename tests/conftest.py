from collections import namedtuple

import pytest

from dependencies.spark import start_spark


@pytest.fixture(scope="session")
def spark_helper():
    spark, log, _ = start_spark()

    helper_dict = {
        "session": spark,
        "logger": log,
    }

    HelperTuple = namedtuple("HelperTuple", helper_dict)

    return HelperTuple(**helper_dict)
