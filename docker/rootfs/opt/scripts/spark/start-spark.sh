#!/bin/bash

export SPARK_MODE="${SPARK_MODE:-master}"
export SPARK_MASTER_URL="${SPARK_MASTER_URL:-spark://spark-master:7077}"
export SPARK_MASTER_PORT="${SPARK_MASTER_PORT:-7077}"
export SPARK_MASTER_WEBUI_PORT="${SPARK_MASTER_WEBUI_PORT:-8080}"
export SPARK_WORKER_WEBUI_PORT="${SPARK_WORKER_WEBUI_PORT:-8080}"
export SPARK_WORKER_PORT="${SPARK_WORKER_PORT:-7000}"

if [ "$SPARK_MODE" == "master" ]; then

    export SPARK_MASTER_HOST=$(hostname)

    /opt/spark/bin/spark-class org.apache.spark.deploy.master.Master \
        --ip $SPARK_MASTER_HOST \
        --port $SPARK_MASTER_PORT \
        --webui-port $SPARK_MASTER_WEBUI_PORT >>$SPARK_MASTER_LOG

elif [ "$SPARK_MODE" == "worker" ]; then
    # When the spark work_load is worker run class org.apache.spark.deploy.master.Worker
    /opt/spark/bin/spark-class org.apache.spark.deploy.worker.Worker \
        --webui-port $SPARK_WORKER_WEBUI_PORT $SPARK_MASTER_URL >>$SPARK_WORKER_LOG

elif [ "$SPARK_MODE" == "submit" ]; then
    echo "SPARK SUBMIT"

else
    echo "Undefined Workload Type $SPARK_MODE, must specify: master, worker, submit"
fi
