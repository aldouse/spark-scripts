#!/bin/bash

curl https://jdbc.postgresql.org/download/postgresql-42.3.3.jar \
    --output /opt/spark/jars/postgresql-42.3.3.jar

curl https://repo1.maven.org/maven2/mysql/mysql-connector-java/8.0.28/mysql-connector-java-8.0.28.jar \
    --output /opt/spark/jars/mysql-connector-java-8.0.28.jar

curl https://repo1.maven.org/maven2/com/google/cloud/bigdataoss/gcs-connector/hadoop3-2.2.4/gcs-connector-hadoop3-2.2.4-shaded.jar \
    --output /opt/spark/jars/gcs-connector-hadoop3-2.2.4-shaded.jar
    
curl https://repo1.maven.org/maven2/com/microsoft/sqlserver/mssql-jdbc/11.2.1.jre8/mssql-jdbc-11.2.1.jre8.jar \
    --output /opt/spark/jars/mssql-jdbc-11.2.1.jre8.jar
