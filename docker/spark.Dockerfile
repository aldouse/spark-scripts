FROM python:3.9.10-slim-bullseye

RUN apt-get update
RUN apt-get install build-essential software-properties-common -y

# Utils
RUN apt-get install -y --no-install-recommends \
        wget \
        curl \
        procps \
        unzip

# Install java
RUN wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add -
RUN add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
RUN apt-get update && apt-get install adoptopenjdk-11-hotspot -y

# Install spark
RUN curl https://archive.apache.org/dist/spark/spark-3.2.2/spark-3.2.2-bin-hadoop3.2.tgz -o /tmp/spark-3.2.2-bin-hadoop3.2.tgz
RUN tar xvf /tmp/spark-3.2.2-bin-hadoop3.2.tgz -C /opt
RUN mv /opt/spark-3.2.2-bin-hadoop3.2 /opt/spark

COPY requirements /tmp/requirements
RUN pip install -r /tmp/requirements.txt --use-deprecated legacy-resolver

COPY rootfs /
RUN chmod +x /opt/scripts/spark/setup-spark.sh && sh /opt/scripts/spark/setup-spark.sh

ENV PATH="/opt/spark/bin:/opt/spark/sbin:$PATH" \
  SPARK_HOME="/opt/spark" \
  JAVA_HOME="/usr/lib/jvm/adoptopenjdk-11-hotspot-amd64" \
  SPARK_LOG_DIR=/opt/spark/logs \
  SPARK_MASTER_LOG=/opt/spark/logs/spark-master.out \
  SPARK_WORKER_LOG=/opt/spark/logs/spark-worker.out

RUN cp /opt/scripts/spark/conf/spark-defaults.conf /opt/spark/conf/spark-defaults.conf

EXPOSE 8080 7077 6066

RUN mkdir -p $SPARK_LOG_DIR
RUN touch $SPARK_MASTER_LOG
RUN touch $SPARK_WORKER_LOG
RUN ln -sf /dev/stdout $SPARK_MASTER_LOG
RUN ln -sf /dev/stdout $SPARK_WORKER_LOG

RUN chmod +x /opt/scripts/spark/start-spark.sh
CMD ["/bin/bash", "/opt/scripts/spark/start-spark.sh"]
