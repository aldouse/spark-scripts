import os
import sys
import time
from argparse import Namespace
from datetime import datetime

from simple_parsing import ArgumentParser

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from dependencies.jobs import SimpleJob
from dependencies.options import GCSOptions, JDBCOptions, SparkResourceEstimatorOptions
from dependencies.sinks import GCSSink
from dependencies.sources import (
    JDBCMysqlSource,
    JDBCPostgresqlSource,
    JDBCSqlServerSource,
)
from dependencies.spark import start_spark
from dependencies.transforms import AddProcessedTimestamp
from dependencies.utils.constants import DatabaseType
from dependencies.utils.spark_resource_estimator import JDBCSourceSparkResourceEstimator


def _job_arguments() -> Namespace:
    parser = ArgumentParser()
    parser.add_arguments(JDBCOptions, dest="jdbc_options", prefix="jdbc_")
    parser.add_arguments(GCSOptions, dest="gcs_options", prefix="gcs_")
    parser.add_arguments(
        SparkResourceEstimatorOptions, dest="sre_options", prefix="sre_"
    )

    parser.add_argument(
        "--timestamp_column",
        help="column_name that will be used for interval based filtering, if left empty will not generate where clause",
        default="",
        type=str,
    )

    parser.add_argument(
        "--timestamp_filter",
        help="timestamp that will be used in filtering provided timestamp_column",
        default=datetime.utcfromtimestamp(time.time()),
        type=datetime.fromisoformat,
    )

    parser.add_argument(
        "--interval_in_hour",
        help="hour interval that will be used to filter the upper ceiling of timestamp_filter",
        default="6",
        type=str,
    )

    parser.add_argument(
        "--processed_timestamp",
        help="timestamp of spark job run time",
        default=datetime.utcfromtimestamp(time.time()),
        type=datetime.fromisoformat,
    )

    parser.add_argument(
        "--use_resource_estimator",
        help="if set to True, specified spark specification will be overridden by resource estimator",
        default="false",
        type=str,
    )

    args = parser.parse_args()

    return args


def main():
    args = _job_arguments()

    spark_config: dict = {
        "spark.sql.parquet.outputTimestampType": "TIMESTAMP_MILLIS",
        "spark.sql.parquet.datetimeRebaseModeInWrite": "CORRECTED",
    }

    if str(args.use_resource_estimator).lower() in ["true", "t", "1"]:
        resource_estimator: JDBCSourceSparkResourceEstimator = (
            JDBCSourceSparkResourceEstimator(
                args.jdbc_options,
                args.sre_options,
                args.jdbc_options.query,
            )
        )
        estimated_resources: dict = resource_estimator.get_spark_conf()
        spark_config["spark.executor.memory"] = estimated_resources[
            "spark.executor.memory"
        ]
        spark_config["spark.executor.cores"] = estimated_resources[
            "spark.executor.cores"
        ]
        spark_config["spark.cores.max"] = estimated_resources["spark.cores.max"]

        args.jdbc_options.partition_column = estimated_resources[
            "spark_reader.partitionColumn"
        ]
        args.jdbc_options.num_partitions = estimated_resources[
            "spark_reader.numPartitions"
        ]
        args.jdbc_options.fetch_size = estimated_resources["spark_reader.fetchSize"]

    spark, logger, _ = start_spark(
        app_name=f"db_to_gcs_job: {args.jdbc_options.db_name}.{args.jdbc_options.table_name}",
        spark_config=spark_config,
    )

    logger.info("Initializing simple database to gcs spark job")

    if args.jdbc_options.db_type == DatabaseType.MYSQL.value:
        data_source = JDBCMysqlSource(spark, logger, args.jdbc_options)
    elif args.jdbc_options.db_type == DatabaseType.POSTGRESQL.value:
        data_source = JDBCPostgresqlSource(spark, logger, args.jdbc_options)
    elif args.jdbc_options.db_type == DatabaseType.SQLSERVER.value:
        data_source = JDBCSqlServerSource(spark, logger, args.jdbc_options)
    else:
        raise Exception(
            f"Database type of {args.jdbc_options.db_type} is not supported"
        )

    data_sink = GCSSink(spark, logger, args.gcs_options)

    add_processed_timestamp_transformation = AddProcessedTimestamp(
        logger, args.processed_timestamp
    )

    db_to_gcs_job = SimpleJob(
        spark,
        logger,
        data_source=data_source,
        data_sink=data_sink,
        transforms=[add_processed_timestamp_transformation],
    )

    db_to_gcs_job.run()


if __name__ == "__main__":
    main()
