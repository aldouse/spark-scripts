from dependencies.sources import JDBCMysqlSource
from dependencies.spark import start_spark


def main():
    spark, log, config = start_spark(
        app_name="test_spark",
    )
